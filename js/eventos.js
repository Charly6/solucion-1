import json from "../mock/pellis.json" assert { type: "json" }
/** 
console.log(json);

const div = document.createElement("div")

const app = document.querySelector("#peliculas")

app.insertAdjacentElement("beforeend", div)
*/
const app = document.querySelector("#peliculas")

json.peliculas.map(peli => {
  const element = document.createElement("div");
  element.classList.add("pelicula");
  
  const img = document.createElement("img");
  img.src = peli.imagen;
  
  const title = document.createElement("h4");
  title.textContent = peli.titulo;
  
  const descripcion = document.createElement("p");
  descripcion.textContent = peli.descripcion;

  element.insertAdjacentElement("beforeend", img)
  element.insertAdjacentElement("beforeend", title)
  element.insertAdjacentElement("beforeend", descripcion)

  app.insertAdjacentElement("beforeend", element)

  //console.log(element);
})